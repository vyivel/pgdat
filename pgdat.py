import sys
import struct


def to_u32(data: bytes) -> int:
    return struct.unpack("<I", data)[0]


def main():
    if len(sys.argv) != 2:
        print(f"usage: {sys.argv[0]} <file>")
        exit(1)

    name = sys.argv[1]
    with open(name, mode="rb") as dat:
        data = dat.read()

    def get_items(start: int, n: int, size: int = 4) -> list[bytes]:
        return [data[start + i * size : start + (i + 1) * size] for i in range(n)]

    [
        magic,
        file_count,
        file_table_offset,
        _,
        name_table_offset,
        size_table_offset,
        _,
        _,
    ] = get_items(0, 8)

    if magic != b"DAT\x00":
        print("Bad magic!")
        exit(1)

    file_count = to_u32(file_count)
    file_table_offset = to_u32(file_table_offset)
    name_table_offset = to_u32(name_table_offset)
    size_table_offset = to_u32(size_table_offset)

    starts = get_items(file_table_offset, file_count)
    starts = [to_u32(start) for start in starts]

    name_table_alignment = to_u32(*get_items(name_table_offset, 1))
    names = get_items(name_table_offset + 4, file_count, name_table_alignment)
    names = [name.decode().rstrip("\x00") for name in names]

    sizes = get_items(size_table_offset, file_count)
    sizes = [to_u32(size) for size in sizes]

    print(f"{file_count} file(s): ")
    for name, start, size in zip(names, starts, sizes):
        print(name)
        with open(name, "wb") as out:
            out.write(data[start : start + size])


if __name__ == "__main__":
    main()
