# pgdat

Platinum Games .dat extractor

## Usage

```
python pgdat.py <file>
```

## License

0BSD

See `LICENSE` for more information.